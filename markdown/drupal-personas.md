---
title: Drupal Personas
revealjs-url: 'reveal.js'
theme: solarized
---

# Introduction

## Drupal Personas

- Administrator
- Site Builder
- Content Manager
- Content Editor
- Anonymous

# Administrator

## Goal

Responsible for administering a Drupal site

## Behaviors

* A big part of their day-to-day work is Drupal related.
* Keeps track of the status of the site, like database or server problems, or module or security updates needed.
* Updates the site and its contributed modules.
* Develop new features by writing modules
* Administers users and permissions
* Troubleshooting, like clearing cache or run cron

## Motivations

* As the responsible of the site, wants to keep it working
* Is responsible to the site owner

## Attitudes

- todo

## Frustrations

* Unmaintained projects and unstable releases
* Unclear communication of project status
* The admin UI is complicated
* The options are confusing.

## Tech Use

* Git, GitHub, Bitbucket
* Console
* Drush
* Devops? Servers, databases…
* Laptop, sometimes additional monitor
* Cloud platform (AWS, Google, Azure)

# Site Builder

## Goal

Build features on a Drupal site though the UI

## Behaviors

* Most part of their day-to-day work is Drupal related
* Administers users and permissions
* Translates User Interface
* Adds content types---configure mix of new and old fields
* Develop twig templates and css for new content types
* Documents functionality for content users.
* Add views to display new content and modify existing views so new content is included.

## Motivations

* As the responsible of the site, want to keep it working
* Wants to be able to implement new features without too much involvement from the Administrators

## Attitudes

- todo

## Frustrations

* Doesn’t have enough UX knowledge to know which is the best disposition of items in a form

## Tech Use

* Browser, mainly UI
* Uses issue tracking tools to manage work to do (Basecamp, Jira, PivotalTracker, Trello, Alfresco for doc management)
* Laptop, sometimes additional monitor, Iphone, Ipad

# Content Manager

## Goal

Manages content on a Drupal site

## Behaviors

* Publishes content
* Review the content before it gets published
* Interacts with other people content

## Motivations

- todo

## Attitudes

- todo

## Frustrations

* The information architecture of the site doesn’t make any sense

## Tech Use

* Browser, mainly UI
* Uses issue tracking tools to manage work to do (Basecamp, Jira, PivotalTracker, Trello, Alfresco for doc management)
* Laptop, sometimes additional monitor, Iphone, Ipad

# Content Editor

## Goal

Creates content on a Drupal site

## Behaviors

* A big part of their time is related to creating content
* Translates content
* Interacts its own content

## Motivations

* Create quality content

## Attitudes

- todo

## Frustrations

* The information architecture of the site doesn’t make any sense
* Image management can be fragile — find and reusing previous images, choosing the proper image types (jpg vs png) and sizes. 
* The site is slow
* The admin UI is complicated
* The options are confusing.

## Tech Use

* Browser, mainly UI
* Uses issue tracking tools to manage work to do (Basecamp, Jira, PivotalTracker, Trello, Alfresco for doc management)
* Laptop, sometimes additional monitor, Iphone, Ipad

# Anonymous

## Behaviors

- todo

## Motivations

- todo

## Attitudes

- todo

## Frustrations

- todo

## Tech Use

- todo


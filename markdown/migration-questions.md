---
title: "Migrate all the things, ask all the questions"
revealjs-url: 'reveal.js'
theme: solarized
---

# Introduction

## Goals

I am starting a migration project. How do I start planning? What questions
should I ask?

## Summary

- It's all about the source, about the source.
- Lots of options, lots of decisions
- The basics
- The nitty-gritty

# It's all about the source, about the source

## My mantra

The most important part of any migration project is understanding the source.

## Plan Ahead

- What does the site do?
- What does it do well?
- What needs to be improved?

Planning ahead will help you

- Have a better site at the end
- Avoid migrating everything "because it is there"

## Mind your grammar

What is the vocabulary of your site?

- Nouns: things (entities)
- Adjectives: properties of things (fields)
- Verbs: actions, business rules (administration, forms)
- Adverbs: restrictions (roles, permissions)

## Build back better

- What could not be done when the original site was built?
- What worked in the past?
- What do we want to improve?
- What challenges did the old site have?
- How could it have been done better?

## What is hiding under the bed?

All the stuff your shiny, new site does not have:

- Change your mind
- Replicated structure from previous migrations
- Undocumented features
- Forgotten experiments
- Trojan horses

If only there were one word ...

# Lots of options, lots of decisions

## Keep, kill, combine

- The more we leave behind, the sooner we finish.
- Replicate the original or restructure?

## Identity

Do we preserve entity IDs or do we create new ones?

## Timing is everything

- All at once or incremental?
- Content freeze?
- How long will it take?

# The basics

## Size matters

How many items do we have?

- Nodes
- Files
- Users
- Taxonomy
- Commerce (products, orders, invoices)

## Speak my language?

Is the site multilingual?

## History

Do we care about preserving revision history?

> Those who forget history ...

# The nitty-gritty

## Size is not everything

What is the content structure?

- Simple: nodes with text fields
- Complex: nested paragraphs, field collections, blocks
- How deep do the references go?

## Lions and Tigers and ...

What is in your text fields?

- Text and HTML tags
- Links
  - external
  - internal, like `/node/123`
  - internal, like `/blog/what-i-learned`
- Tokens (media, references)
  - How are they processed?

# Conclusion

## Summary

- It's all about the source, about the source.
- Lots of options, lots of decisions
- The basics
- The nitty-gritty

## Credits

Some of the ideas in this document come from a discussion in the `#migration`
channel on Drupal Slack, started 2023-02-03. Contributors include

- `@Bisonbleu`
- `@Will Kirchheimer`
- `@Scott Sawyer`
